﻿using System;
using iTextSharp.text.pdf;
using Org.BouncyCastle.X509;
using Twenty57.Linx.Components.Pdf.ChangeProtection.Runtime;
using Twenty57.Linx.Plugin.Common;
using Twenty57.Linx.Plugin.Common.CodeGeneration;

namespace Twenty57.Linx.Components.Pdf.ChangeProtection
{
    internal class CodeGenerator : FunctionCodeGenerator
    {
        public CodeGenerator(IFunctionData data) : base(data) { }

        public override void GenerateCode(IFunctionBuilder functionBuilder)
        {
            functionBuilder.AddAssemblyReference(GetType());
            functionBuilder.AddAssemblyReference(typeof(Uri));
            functionBuilder.AddAssemblyReference(typeof(PdfReader));
            functionBuilder.AddAssemblyReference(typeof(X509Certificate));

            ChangeProtectionTemplate template = new ChangeProtectionTemplate();
            template.Populate(functionBuilder, FunctionData);
            functionBuilder.AddCode(template.TransformText());
        }
    }
}