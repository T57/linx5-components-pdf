﻿using Twenty57.Linx.Plugin.Common;

namespace Twenty57.Linx.Components.Pdf.ChangeProtection
{
    public class Provider : FunctionProvider
    {
        public override string Name => "ChangeProtection";

        public override string SearchKeywords => "pdf change protection";

        public override FunctionCodeGenerator CreateCodeGenerator(IFunctionData data) => new CodeGenerator(data);

        public override FunctionDesigner CreateDesigner(IFunctionData data, IDesignerContext context) => new Designer(data, context);

        public override FunctionDesigner CreateDesigner(IDesignerContext context) => new Designer(context);
    }
}