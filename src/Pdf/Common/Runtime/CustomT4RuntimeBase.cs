﻿using System;
using Twenty57.Linx.Plugin.Common;
using Twenty57.Linx.Plugin.Common.CodeGeneration;

namespace Twenty57.Linx.Components.Pdf.Common.Runtime
{
    internal abstract class CustomT4RuntimeBase : T4RuntimeBase
    {
        public abstract void Populate(IFunctionBuilder functionBuilder, IFunctionData functionData);
        public virtual string TransformText() => throw new NotImplementedException();
        public virtual void Initialize() { }
    }
}
