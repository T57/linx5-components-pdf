﻿using Twenty57.Linx.Components.Pdf.Common.Runtime;
using Twenty57.Linx.Plugin.Common;
using Twenty57.Linx.Plugin.Common.CodeGeneration;

namespace Twenty57.Linx.Components.Pdf.PdfOperations.Runtime
{
    internal partial class ConcatenateTemplate : CustomT4RuntimeBase
    {
        public string InputFilesParameterName { get; private set; }
        public string OutputFilePathParameterName { get; private set; }
        public string ContextParameterName { get; private set; }

        public override void Populate(IFunctionBuilder functionBuilder, IFunctionData functionData)
        {
            InputFilesParameterName = functionBuilder.GetParamName(PropertyNames.InputFiles);
            OutputFilePathParameterName = functionBuilder.GetParamName(PropertyNames.OutputFilePath);
            ContextParameterName = functionBuilder.ContextParamName;
        }
    }
}