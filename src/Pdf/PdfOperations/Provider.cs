﻿using System;
using Twenty57.Linx.Plugin.Common;

namespace Twenty57.Linx.Components.Pdf.PdfOperations
{
    [Obsolete]
    public class Provider : FunctionProvider
    {
        private static readonly string searchKeywords;

        static Provider()
        {
            searchKeywords = "pdf " + string.Join(" ", Enum.GetNames(typeof(Operation)));
        }

        public override string Name => "PdfOperations";

        public override string SearchKeywords => searchKeywords;

        public override FunctionDesigner CreateDesigner(IDesignerContext context) => new Designer(context);

        public override FunctionDesigner CreateDesigner(IFunctionData data, IDesignerContext context) => new Designer(data, context);

        public override FunctionCodeGenerator CreateCodeGenerator(IFunctionData data) => new CodeGenerator(data);

        public override bool TryUpdateToLatestVersion(IFunctionData data, IUpdateContext context, out IFunctionData updatedData) => Updater.Instance.TryUpdate(data, context, out updatedData);
    }
}