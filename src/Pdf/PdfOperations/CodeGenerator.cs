﻿using System;
using System.Drawing.Drawing2D;
using iTextSharp.text.pdf;
using Org.BouncyCastle.X509;
using Twenty57.Linx.Components.Pdf.Common.Runtime;
using Twenty57.Linx.Components.Pdf.PdfOperations.Runtime;
using Twenty57.Linx.Plugin.Common;
using Twenty57.Linx.Plugin.Common.CodeGeneration;

namespace Twenty57.Linx.Components.Pdf.PdfOperations
{
    internal class CodeGenerator : FunctionCodeGenerator
    {
        public CodeGenerator(IFunctionData data)
            : base(data)
        { }

        public override void GenerateCode(IFunctionBuilder functionBuilder)
        {
            functionBuilder.AddAssemblyReference(GetType());
            functionBuilder.AddAssemblyReference(typeof(Uri));
            functionBuilder.AddAssemblyReference(typeof(PdfReader));

            Operation operationValue = FunctionData.Properties[PropertyNames.Operation].GetValue<Operation>();
            switch (operationValue)
            {
                case Operation.FillForm:
                    GenerateTemplateCode<FillFormTemplate>(functionBuilder);
                    break;
                case Operation.Protect:
                    functionBuilder.AddAssemblyReference(typeof(X509Certificate));
                    GenerateTemplateCode<ProtectTemplate>(functionBuilder);
                    break;
                case Operation.Split:
                    functionBuilder.AddAssemblyReference(typeof(X509Certificate));
                    GenerateTemplateCode<SplitTemplate>(functionBuilder);
                    break;
                case Operation.Concatenate:
                    GenerateTemplateCode<ConcatenateTemplate>(functionBuilder);
                    break;
                case Operation.AddWatermark:
                    functionBuilder.AddAssemblyReference(typeof(Matrix));
                    functionBuilder.AddAssemblyReference(typeof(X509Certificate));
                    GenerateTemplateCode<AddWatermarkTemplate>(functionBuilder);
                    break;
                case Operation.Sign:
                    functionBuilder.AddAssemblyReference(typeof(X509Certificate));
                    GenerateTemplateCode<SignTemplate>(functionBuilder);
                    break;
                default:
                    throw new NotSupportedException("Invalid Operation specified.");
            }
        }

        private void GenerateTemplateCode<T>(IFunctionBuilder functionBuilder) where T : CustomT4RuntimeBase
        {
            T template = Activator.CreateInstance<T>();
            template.Populate(functionBuilder, FunctionData);
            functionBuilder.AddCode(template.TransformText());
        }
    }
}
