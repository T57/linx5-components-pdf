﻿using Twenty57.Linx.Plugin.Common;

namespace Twenty57.Linx.Components.Pdf.Concatenate
{
    public class Provider : FunctionProvider
    {
        public override string Name => "Concatenate";

        public override string SearchKeywords => "pdf concatenate";

        public override FunctionCodeGenerator CreateCodeGenerator(IFunctionData data) => new CodeGenerator(data);

        public override FunctionDesigner CreateDesigner(IFunctionData data, IDesignerContext context) => new Designer(data, context);

        public override FunctionDesigner CreateDesigner(IDesignerContext context) => new Designer(context);
    }
}