﻿using System.ComponentModel;

namespace Twenty57.Linx.Components.Pdf.Read
{
    public enum TextExtractionStrategy
    {
        Location,
        Simple,
        [Description("Top to bottom")]
        TopToBottom
    }
}