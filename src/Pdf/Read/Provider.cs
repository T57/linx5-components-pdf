using Twenty57.Linx.Plugin.Common;

namespace Twenty57.Linx.Components.Pdf.Read
{
    public class Provider : FunctionProvider
    {
        public override string Name => "Read";

        public override string SearchKeywords => "pdf read text form signature";

        public override FunctionDesigner CreateDesigner(IDesignerContext context) => new Designer(context);

        public override FunctionDesigner CreateDesigner(IFunctionData data, IDesignerContext context) => new Designer(data, context);

        public override FunctionCodeGenerator CreateCodeGenerator(IFunctionData data) => new CodeGenerator(data);

        public override bool TryUpdateToLatestVersion(IFunctionData data, IUpdateContext context, out IFunctionData updatedData) => Updater.Instance.TryUpdate(data, context, out updatedData);
    }
}